This app packages [home-cloudron](https://github.com/tomershvueli/homepage-cloudron) <upstream>v0.9.1</upstream>

A simple, standalone, self-hosted PHP page that is your window to your server and the web. This version is configured specifically for Cloudron such that given your Cloudron's URL and a proper access token, it will auto-populate with your installed applications. 

### Features

* Simple, beautiful wallpapers from Unsplash, or customized given a URL
* Auto populate app links and icons from your Cloudron instance
* Quick link to your main Cloudron dashboard
