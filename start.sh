#!/bin/bash

set -eu

mkdir -p /app/data
mkdir -p /run/app/sessions

if [[ ! -e /app/data/img_cache ]]; then
    echo "==> Creating image cache directory"
    cp -r /app/code/src/hp_assets/img/cache.orig /app/data/img_cache
else
    echo "==> Clearing out image cache directory"
    rm -f /app/data/img_cache/*.png
fi

echo "==> Creating config"
if [[ ! -f /app/data/config.json ]]; then
    cp /app/code/src/config.sample.json /app/data/config.json

    # Set the main Cloudron URL based on the environment variable
    sed -i "s#\"cloudron_api_url\": \"\"#\"cloudron_api_url\": \"${CLOUDRON_API_ORIGIN}\"#" /app/data/config.json
fi

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/

echo "==> Staring Homepage"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
