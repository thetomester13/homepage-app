#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent');

var By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder,
    Driver = require('selenium-webdriver').Driver,
    Key = require('selenium-webdriver').Key;

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';

    var server, browser = new Builder().forBrowser('chrome').build();
    var app;

    before(function () {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();
    });

    after(function () {
        browser.quit();
    });

    function canToggleMenu(done) {
        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(By.id('links-wrap')).isDisplayed();
        }).then(function(menuVisible) {
            if (menuVisible) return Promise.reject(new Error("Menu is visible when it shouldn't be"));
        }).then(function() {
            return browser.findElement(By.tagName("body")).sendKeys(Key.SPACE);
        }).then(function() {
            return browser.sleep(1000);
        }).then(function() {
        return browser.findElement(By.id('links-wrap')).isDisplayed();
        }).then(function(menuVisible) {
          if (!menuVisible) return Promise.reject(new Error("Menu is not visible when it should be"));
        }).then(function() {
            return browser.findElement(By.tagName("body")).sendKeys(Key.SPACE);
        }).then(function() {
            return browser.sleep(1000);
        }).then(function() {
        return browser.findElement(By.id('links-wrap')).isDisplayed();
        }).then(function(menuVisible) {
          if (menuVisible) return Promise.reject(new Error("Menu is visible when it shouldn't be"));
        }).then(function() {
            done();
        });
    }

    it('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can use menu file', canToggleMenu);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can use menu file', canToggleMenu);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can use menu file', canToggleMenu);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install previous version from appstore', function () {
        execSync(`cloudron install --appstore-id info.privatebin.cloudronapp --location ${LOCATION}`);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can use menu file', canToggleMenu);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
