# Homepage Cloudron App

This repository contains the Cloudron app package source for [homepage](https://github.com/tomershvueli/homepage-cloudron).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd homepage-app

cloudron build
cloudron install
```

## Notes

* Homepage requires you to input a manually generated API access token into the `/app/data/config.json` file. You can get an API access token by visiting your Cloudron dashboard and adding an /tokens to the end of the url instead of /apps, i.e. my.example.com/#/tokens. 
