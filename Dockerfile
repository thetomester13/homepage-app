FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ENV PROJECT_REPO=https://github.com/tomershvueli/homepage-cloudron
ENV RELEASE=v0.9.6

EXPOSE 8000

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN curl -L "${PROJECT_REPO}/archive/${RELEASE}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code \
    && ln -s /app/data/config.json /app/code/src/config.json \
    && mv /app/code/src/hp_assets/img/cache /app/code/src/hp_assets/img/cache.orig \
    && ln -s /app/data/img_cache /app/code/src/hp_assets/img/cache

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/homepage.conf /etc/apache2/sites-enabled/homepage.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Configure mod_php
RUN a2enmod php7.3 headers

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
